# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

#[=======================================================================[.rst:
FindOpenSSL2
------------

Find the OpenSSL2 encryption library. Tries to make it in generic way assuming unknown
number of components and their dependencies in most of the code.

Optional COMPONENTS
^^^^^^^^^^^^^^^^^^^

This module supports two optional COMPONENTS: ``Crypto`` and ``SSL``.  Both
components have associated imported targets, as described below.

Imported Targets
^^^^^^^^^^^^^^^^

This module defines the following :prop_tgt:`IMPORTED` targets:

``OpenSSL2::SSL``
  The OpenSSL ``ssl`` library, if found.
``OpenSSL2::Crypto``
  The OpenSSL ``crypto`` library, if found.

Result Variables
^^^^^^^^^^^^^^^^

This module will set the following variables in your project:

``OPENSSL2_FOUND``
  System has the OpenSSL library. If no components are requested it only
  requires the crypto library.
``OPENSSL2_INCLUDE_DIR``
  The OpenSSL include directory.

Hints
^^^^^

Set ``OPENSSL2_ROOT_DIR`` to the root directory of an OpenSSL installation.
Set ``OPENSSL2_USE_STATIC_LIBS`` to ``TRUE`` to look for static libraries.
Set ``OPENSSL2_FINDPKG_DEBUG`` to print additional info while looking for the package.
#]=======================================================================]

# This sample script tries to handle openssl components' dependencies in generic way regardless of
# the fact that this bundle has only two components and one depends on another. That's why the code
# may look too complicated for this specific case. That's why current implementation looks
# horrible and I'm not going to offer it to anybody.

# Things left out so far:
#  * version handling
#  * debug/release support for multiconfiguration targets like MS Visual Studio generator

if(UNIX)
  # Info from pkg-config will be used as a prompt about openssl bundle and its components placement.
  find_package(PkgConfig QUIET)
  if(PKG_CONFIG_FOUND)
    pkg_check_modules(_OPENSSL2_PKG QUIET openssl)
    pkg_check_modules(_LIBSSL2_PKG QUIET libssl)
    pkg_check_modules(_LIBCrypto2_PKG QUIET libcrypto)
  endif()
endif()

if(OPENSSL2_USE_STATIC_LIBS)
  set(_OPENSSL2_PKG_INCDIR_HINT ${_OPENSSL2_PKG_STATIC_INCLUDE_DIRS})
else()
  set(_OPENSSL2_PKG_INCDIR_HINT ${_OPENSSL2_PKG_INCLUDE_DIRS})
endif()
set(_OPENSSL2_INCDIR_HINTS ${OPENSSL2_ROOT_DIR} ${_OPENSSL2_PKG_INCDIR_HINT})

# Find the main library's header - it will help to determine a root directory
find_path(OPENSSL2_INCLUDE_DIR
  NAMES openssl/ssl.h
  HINTS ${_OPENSSL2_INCDIR_HINTS}
  PATH_SUFFIXES include
)

# Check whether the main header file found in a directory provided by pkg-config. In this case
# dependencies info is read from pkg-config also. Else check whether a user-provided hint for root
# directory helped...
set(_OPENSSL2_FOUND_WITH_PKG 0)
set(_OPENSSL2_FOUND_WITH_ROOT_HINT 0)
string(FIND "${OPENSSL2_INCLUDE_DIR}" "${_OPENSSL2_PKG_INCDIR_HINT}" _OPENSSL2_IDX)
if(_OPENSSL2_PKG_INCDIR_HINT AND _OPENSSL2_IDX STREQUAL 0)
  set(_OPENSSL2_FOUND_WITH_PKG 1)
else()
  string(FIND "${OPENSSL2_INCLUDE_DIR}" "${OPENSSL2_ROOT_DIR}" _OPENSSL2_IDX)
  if(OPENSSL2_ROOT_DIR AND _OPENSSL2_IDX STREQUAL 0)
    set(_OPENSSL2_FOUND_WITH_ROOT_HINT 1)
  endif()
endif()

if(OPENSSL2_FINDPKG_DEBUG)
  message("FindOpenSSL2: found with pkg-config = ${_OPENSSL2_FOUND_WITH_PKG}, found with root hint = ${_OPENSSL2_FOUND_WITH_ROOT_HINT}")
endif()

# Setup a library's extension for further searching of libraries depending on static/shared required libs.
if(OPENSSL2_USE_STATIC_LIBS)
  set(_OPENSSL2_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES})
  if(WIN32)
    set(CMAKE_FIND_LIBRARY_SUFFIXES .lib .a ${CMAKE_FIND_LIBRARY_SUFFIXES})
  else()
    set(CMAKE_FIND_LIBRARY_SUFFIXES .a)
  endif()
endif()

# Hard-coded knowledge about each component's content (which libraries it consists of). The first
# one is considered "main library". And the only in most cases. If the script is run on *nix
# platform and pkg-config helped to run the bundle, dependency information will be reconstructed
# from its output instead of this function.
function(_openssl2_get_cmpt_libnames_and_flags cmpt libs_ret flags_ret)
  # TODO: invalid set of libraries for non-unix platforms. Where do I get
  #       this information from?
  if("${cmpt}" STREQUAL "SSL")
    if(OPENSSL2_USE_STATIC_LIBS)
      set(${libs_ret} ssl crypto PARENT_SCOPE)
      set(${flags_ret} pthread PARENT_SCOPE)
    else()
      set(${libs_ret} ssl PARENT_SCOPE)
    endif()
  elseif("${cmpt}" STREQUAL "Crypto")
    if(OPENSSL2_USE_STATIC_LIBS)
      set(${libs_ret} crypto dl PARENT_SCOPE)
      set(${flags_ret} pthread PARENT_SCOPE)
    else()
      set(${libs_ret} crypto PARENT_SCOPE)
    endif()
  endif()
endfunction()

# Search for libraries and fill a component's variables with full paths to the libraries.  Do not
# expand "special libraries" like dynamic loader or pthreads (let's a linker finds them with default
# searching procedure)
function(_openssl2_find_component cmpt var_prefix)
  if(_OPENSSL2_FOUND_WITH_PKG)
    set(is_static)
    if(OPENSSL2_USE_STATIC_LIBS)
      set(is_static _STATIC)
    endif()
    set(findlibs_opts NO_DEFAULT_PATH)
    set(findlibs_hints ${_LIB${cmpt}2_PKG${is_static}_LIBRARY_DIRS})
    set(findlibs_names ${_LIB${cmpt}2_PKG${is_static}_LIBRARIES})
    set(libs_other ${LIB${cmpt}2_PKG${is_static}_LDFLAGS_OTHER})
  elseif(_OPENSSL2_FOUND_WITH_ROOT_HINT)
    set(findlibs_opts NO_DEFAULT_PATH)
    set(findlibs_hints ${OPENSSL2_ROOT_DIR}/lib)
    _openssl2_get_cmpt_libnames_and_flags(${cmpt} findlibs_names libs_other)
  else()
    set(findlibs_opts)
    set(findlibs_hints)
    _openssl2_get_cmpt_libnames_and_flags(${cmpt} findlibs_names libs_other)
  endif()

  string(REGEX REPLACE "(^|;)-l" "\\1" dlnames "${CMAKE_DL_LIBS}")
  set(libs)

  if(OPENSSL2_FINDPKG_DEBUG)
    message("FindOpenSSL2: looking for libraries for component ${cmpt} ...")
  endif()
  foreach(lib_name IN LISTS findlibs_names)
    # TODO: which "special" libs to ignore besides CMAKE_DL_LIBS?
    if(lib_name IN_LIST dlnames)
      list(APPEND libs ${lib_name})
    else()
      if(OPENSSL2_FINDPKG_DEBUG)
        message("FindOpenSSL2:   > find_library(lib ${lib_name} HINTS ${findlibs_hints} ${findlibs_opts})")
      endif()
      find_library(lib ${lib_name} HINTS ${findlibs_hints} ${findlibs_opts})
      if(lib)
        list(APPEND libs ${lib})
        unset(lib CACHE)
      else()
        set(libs)
        unset(lib CACHE)
        break()
      endif()
    endif()
  endforeach()

  if(libs)
    list(GET libs 0 main_lib)
    list(REMOVE_AT libs 0)
    set(libs ${libs} ${libs_other})
    set(${var_prefix}_${cmpt}_MAIN_LIB ${main_lib} PARENT_SCOPE)
    set(${var_prefix}_${cmpt}_LIBS ${libs} PARENT_SCOPE)
    set(${var_prefix}_${cmpt}_ALL_LIBS ${main_lib} ${libs} PARENT_SCOPE)
  else()
    set(${var_prefix}_${cmpt}_MAIN_LIB "" PARENT_SCOPE)
  endif()
endfunction()

set(_OPENSSL2_ALL_CMPTS SSL Crypto)

# Fill libraries for each component. After executing this statement each component will have next
# variables set:
#   _OPENSSL2_<NAME>_MAIN_LIB - a path to main component's library
#   _OPENSSL2_<NAME>_LIBS - a list of paths to possible additional libraries
#   _OPENSSL2_<NAME>_ALL_LIBS - all the libraries including main one
foreach(_OPENSSL2_CMPT IN LISTS _OPENSSL2_ALL_CMPTS)
  _openssl2_find_component(${_OPENSSL2_CMPT} _OPENSSL2)
  if(OPENSSL2_FINDPKG_DEBUG)
    message("FindOpenSSL2: found component ${_OPENSSL2_CMPT}: main_lib=${_OPENSSL2_${_OPENSSL2_CMPT}_MAIN_LIB}, libs=${_OPENSSL2_${_OPENSSL2_CMPT}_LIBS}")
  endif()
endforeach()

# Detect deps for each component (N^2). Any additional library found in previous step can be really
# the main library from another component. In this case it should be removed from the list of
# additional libraries of the former and the new dependency should be set for it.
# A new optional variable for each compoent is set in this case:
#   _OPENSSL2_<NAME>_DEPS - a list of other components - dependencies
foreach(_OPENSSL2_CMPT IN LISTS _OPENSSL2_ALL_CMPTS)
  foreach(_OPENSSL2_OTHER_CMPT IN LISTS _OPENSSL2_ALL_CMPTS)
    if(_OPENSSL2_CMPT STREQUAL _OPENSSL2_OTHER_CMPT OR
       NOT _OPENSSL2_${_OPENSSL2_OTHER_CMPT}_MAIN_LIB IN_LIST _OPENSSL2_${_OPENSSL2_CMPT}_LIBS)
      continue()
    endif()
    list(REMOVE_ITEM _OPENSSL2_${_OPENSSL2_CMPT}_LIBS ${_OPENSSL2_${_OPENSSL2_OTHER_CMPT}_MAIN_LIB})
    list(APPEND _OPENSSL2_${_OPENSSL2_CMPT}_DEPS ${_OPENSSL2_OTHER_CMPT})
    if(OPENSSL2_FINDPKG_DEBUG)
      message("FindOpenSSL2: found dependency: ${_OPENSSL2_CMPT} -> ${_OPENSSL2_OTHER_CMPT}")
    endif()
  endforeach()
endforeach()

set(OPENSSL2_LIBRARIES)
set(_OPENSSL2_CMPTS_TO_TARGETS)

function(_openssl2_get_all_deps_for_component cmpt cmpts_var)
  set(cmpts)
  if(NOT _OPENSSL2_${cmpt}_MAIN_LIB)
    unset(${cmpts_var} PARENT_SCOPE)
    return()
  endif()
  set(cmpts ${cmpt})
  foreach(dep IN LISTS _OPENSSL2_${cmpt}_DEPS)
    _openssl2_get_all_deps_for_component(${dep} cmpts_inner)
    if(NOT cmpts_inner)
      unset(${cmpts_var} PARENT_SCOPE)
      return()
    endif()
    list(APPEND cmpts ${cmpts_inner})
  endforeach()
  set(${cmpts_var} ${cmpts} PARENT_SCOPE)
endfunction()

# Now build a chain of components and the libraries (note, that user may want to use old-style
# libraries list instead of transitive dependencies).
if(OpenSSL2_FIND_COMPONENTS)
  foreach(_OPENSSL2_CMPT IN LISTS OpenSSL2_FIND_COMPONENTS)
    _openssl2_get_all_deps_for_component(${_OPENSSL2_CMPT} _OPENSSL2_CMPTS)
    if(NOT "${_OPENSSL2_CMPTS}" STREQUAL "")
      set(OpenSSL2_${_OPENSSL2_CMPT}_FOUND 1)
    endif()
    list(APPEND OPENSSL2_LIBRARIES ${_OPENSSL2_${_OPENSSL2_CMPT}_ALL_LIBS})
    list(APPEND _OPENSSL2_CMPTS_TO_TARGETS ${_OPENSSL2_CMPTS})
  endforeach()
else()
  foreach(_OPENSSL2_CMPT IN LISTS _OPENSSL2_ALL_CMPTS)
    if(_OPENSSL2_${_OPENSSL2_CMPT}_MAIN_LIB)
      list(APPEND OPENSSL2_LIBRARIES ${_OPENSSL2_${_OPENSSL2_CMPT}_ALL_LIBS})
    else()
      set(OPENSSL2_LIBRARIES)
      break()
    endif()
  endforeach()
  set(_OPENSSL2_CMPTS_TO_TARGETS ${_OPENSSL2_ALL_CMPTS})
endif()

list(REMOVE_DUPLICATES OPENSSL2_LIBRARIES)
list(REMOVE_DUPLICATES _OPENSSL2_CMPTS_TO_TARGETS)

if(OPENSSL2_FINDPKG_DEBUG)
  message("FindOpenSSL2: targets to define: ${_OPENSSL2_CMPTS_TO_TARGETS}")
endif()

# Now we have a list of targets to define and an info for each target. Let's make imported targets
# for each component.
foreach(_OPENSSL2_CMPT IN LISTS _OPENSSL2_CMPTS_TO_TARGETS)
  if (_OPENSSL2_${_OPENSSL2_CMPT}_LIBS)
    list(REMOVE_DUPLICATES _OPENSSL2_${_OPENSSL2_CMPT}_LIBS)
  endif()
  if(OPENSSL2_FINDPKG_DEBUG)
    message("FindOpenSSL2: define target OpenSSL2::${_OPENSSL2_CMPT} (main_lib=${_OPENSSL2_${_OPENSSL2_CMPT}_MAIN_LIB}, libs=${_OPENSSL2_${_OPENSSL2_CMPT}_LIBS})")
  endif()
  add_library(OpenSSL2::${_OPENSSL2_CMPT} UNKNOWN IMPORTED)
  set_target_properties(OpenSSL2::${_OPENSSL2_CMPT} PROPERTIES
    IMPORTED_LINK_INTERFACE_LANGUAGES "C"
    INTERFACE_INCLUDE_DIRECTORIES "${OPENSSL2_INCLUDE_DIR}"
    IMPORTED_LOCATION "${_OPENSSL2_${_OPENSSL2_CMPT}_MAIN_LIB}")
  if(_OPENSSL2_${_OPENSSL2_CMPT}_LIBS)
    set_property(TARGET OpenSSL2::${_OPENSSL2_CMPT}
        APPEND PROPERTY INTERFACE_LINK_LIBRARIES ${_OPENSSL2_${_OPENSSL2_CMPT}_LIBS})
  endif()
  if(_OPENSSL2_${_OPENSSL2_CMPT}_DEPS)
    foreach(_OPENSSL2_DEP IN LISTS _OPENSSL2_${_OPENSSL2_CMPT}_DEPS)
      set_property(TARGET OpenSSL2::${_OPENSSL2_CMPT}
        APPEND PROPERTY INTERFACE_LINK_LIBRARIES OpenSSL2::${_OPENSSL2_DEP})
    endforeach()
  endif()
endforeach()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OpenSSL2
  REQUIRED_VARS
    OPENSSL2_LIBRARIES
    OPENSSL2_INCLUDE_DIR
    _OPENSSL2_CMPTS_TO_TARGETS
  HANDLE_COMPONENTS
  FAIL_MESSAGE
    "Could NOT find OpenSSL, try to set the path to OpenSSL root folder in the system variable OPENSSL_ROOT_DIR"
)

if(OPENSSL2_USE_STATIC_LIBS)
  set(CMAKE_FIND_LIBRARY_SUFFIXES ${_OPENSSL2_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES})
endif()
